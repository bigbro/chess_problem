#! -*- coding: utf-8 -*-
from __future__ import print_function
import argparse
import copy
import gc
import sys
from itertools import chain


from models import (
    ChessBoard, Queen, Bishop, Rook, Knight, King,
    place_piece_on_board, prettify_board_string)


def gen_start_boards(size_x, size_y, first_piece):
    for x in range(size_x):
        for y in range(size_y):
            board = ChessBoard(size_x, size_y)
            place_piece_on_board(board, first_piece, x, y)
            yield board


def process_rest_pieces(start_board, pieces, num_pieces_yield):
    solutions = {start_board}
    if not pieces:
        yield start_board
    for piece in pieces:
        _solutions, solutions = solutions, set()
        for n, board in enumerate(_solutions):
            if not n % 20000:
                gc.collect()
            for square in board.free_squares - board.threatened_squares:
                _board = copy.copy(board)
                _placed = place_piece_on_board(_board, piece, *square)
                if not _placed:
                    del _board
                    continue
                if _board.num_pieces == num_pieces_yield:
                    yield _board
                else:
                    solutions.add(_board)
                del _board
        gc.collect()


def solve_problem(size_x, size_y, pieces):
    first_piece, other_pieces = pieces[0], pieces[1:]

    solutions = set()
    for board in gen_start_boards(size_x, size_y, first_piece):
        solutions.update(set(
            str(s)
            for s in process_rest_pieces(board, other_pieces, len(pieces))))
    return solutions


def main():
    description = """
    Finds all unique configurations of a set of normal chess
    pieces on a chess board with dimensions M*N where none of the pieces is in
    a position to take any of the others."""
    parser = argparse.ArgumentParser(description=description)

    dimensions = parser.add_argument_group(
        'board dimensions (if not set the board of 5*5 will be used)')
    dimensions.add_argument('-x', '--columns', type=int, default=5,
                            help='columns (x-axis)')
    dimensions.add_argument('-y', '--rows', type=int, default=5,
                            help='rows (y-axis)')

    pieces = parser.add_argument_group('pieces on board')
    pieces.add_argument('-q', '--queens', type=int, default=0,
                        help='number of Queens')
    pieces.add_argument('-b', '--bishops', type=int, default=0,
                        help='number of Bishops')
    pieces.add_argument('-r', '--rooks', type=int, default=0,
                        help='number of Rook')
    pieces.add_argument('-n', '--knights', type=int, default=0,
                        help='number of Knights')
    pieces.add_argument('-k', '--kings', type=int, default=0,
                        help='number of Kings')

    verbosity = parser.add_argument_group('verbosity')
    verbosity.add_argument('--verbose', action='store_true',
                           help='print resulting boards')
    verbosity.add_argument('--num', type=int, default=10,
                           help='number of boards to print')

    errors = []
    args = parser.parse_args()
    if args.columns < 2:
        errors.append('columns must be greater or equal 2')
    if args.rows < 2:
        errors.append('rows must be greater or equal 2')
    pieces = list(chain(
        [Queen] * args.queens,
        [Rook] * args.rooks,
        [Bishop] * args.bishops,
        [Knight] * args.knights,
        [King] * args.kings))
    if not pieces:
        errors.append('at least one piece must be specified')

    if errors:
        print('\nFollowing errors occured:\n')
        for error in errors:
            print('  * {}'.format(error))
        print('\n')
        parser.print_usage()
        sys.exit(1)

    results = solve_problem(args.columns, args.rows, pieces)
    if args.verbose:
        for n, board in enumerate(results):
            if n >= args.num:
                break
            print(prettify_board_string(args.columns, board))
    print(len(results), 'unique configurations found', end=' ')
    if args.num < len(results) and args.verbose:
        print('(first {} are shown above)'.format(args.num))


if __name__ == '__main__':
    main()

#! -*- coding: utf-8 -*-
import sys
from itertools import chain

if sys.version_info[0] == 3:
    xrange = range


class ChessBoard(object):

    def __init__(self, m, n):
        self.size_x = m
        self.size_y = n
        self.num_pieces = 0
        self.squares = [[None for _ in xrange(m)] for _ in xrange(n)]
        self.free_squares = set((x, y) for x in xrange(m) for y in xrange(n))
        self.threatened_squares = set()

    def __str__(self):
        return ''.join([''.join([c or '.' for c in r]) for r in self.squares])

    def __eq__(self, other):
        return self.squares == other.squares

    def __hash__(self):
        return hash(tuple([tuple(row) for row in self.squares]))

    def __copy__(self):
        new_board = empty_copy(self)
        new_board.size_x = self.size_x
        new_board.size_y = self.size_y
        new_board.free_squares = self.free_squares.copy()
        new_board.threatened_squares = self.threatened_squares.copy()
        new_board.squares = [x[:] for x in self.squares]
        new_board.num_pieces = self.num_pieces
        return new_board


class ChessPiece(object):
    string_value = None

    @classmethod
    def get_offsets(cls, board_size, x, y):
        return ()

    @classmethod
    def get_moves(cls, board, x, y):
        if (x, y) in board.threatened_squares:
            return
        moves = [(x, y)]
        board_size = (board.size_x, board.size_y)
        for delta_x, delta_y in cls.get_offsets(board_size, x, y):
            move = x + delta_x, y + delta_y
            if square_on_board(board_size, *move):
                if move in board.free_squares:
                    moves.append(move)
                else:
                    return
        return moves


class King(ChessPiece):
    string_value = 'K'

    @classmethod
    def get_offsets(cls, board_size, x, y):
        return ((-1, -1), (-1, 0), (-1, 1), (0, -1),
                (0, 1), (1, -1), (1, 0), (1, 1))


class Queen(ChessPiece):
    string_value = 'Q'

    @classmethod
    def get_offsets(cls, board_size, x, y):
        return chain(Rook.get_offsets(board_size, x, y),
                     Bishop.get_offsets(board_size, x, y))


class Bishop(ChessPiece):
    string_value = 'B'

    @classmethod
    def get_offsets(cls, board_size, x, y):
        size_x, size_y = board_size
        offsets = []
        for row in xrange(size_y):
            delta = abs(y - row)
            offsets.append((delta * -1, row - y))
            offsets.append((delta, row - y))
        return offsets


class Rook(ChessPiece):
    string_value = 'R'

    @classmethod
    def get_offsets(cls, board_size, x, y):
        size_x, size_y = board_size
        horizontal = [(row - x, 0) for row in xrange(size_x)]
        vertical = [(0, col - y) for col in xrange(size_y)]
        return chain(horizontal, vertical)


class Knight(ChessPiece):
    string_value = 'N'

    @classmethod
    def get_offsets(cls, board_size, x, y):
        return ((-2, -1), (-2, 1), (-1, -2), (-1, 2),
                (1, -2), (1, 2), (2, -1), (2, 1))


def empty_copy(obj):
    class Empty(obj.__class__):
        def __init__(self):
            pass

    newcopy = Empty()
    newcopy.__class__ = obj.__class__
    return newcopy


def place_piece_on_board(board, piece, x, y):
    moves = piece.get_moves(board, x, y)
    if not moves:
        return False
    board.free_squares.difference_update({(x, y)})
    board.threatened_squares.update(moves)
    # we need to set the board coordinates in this order
    # as rows come first.
    board.squares[y][x] = str(piece.string_value)
    board.num_pieces += 1
    return True


def square_on_board(board_size, x, y):
    size_x, size_y = board_size
    return all((x >= 0, y >= 0, x < size_x, y < size_y))


def prettify_board_string(size_x, board_string):

    def chunks(txt):
        for i in xrange(0, len(txt), size_x):
            yield txt[i:i + size_x]

    horiz_size_x = [u'───'] * size_x
    upper_box_line = u'┌{}┐\n'.format(u'┬'.join(horiz_size_x))
    lower_box_line = u'└{}┘\n'.format(u'┴'.join(horiz_size_x))
    middle_box_line = u'├{}┤\n'.format(u'┼'.join(horiz_size_x))
    content = middle_box_line.join(
        [u'│' + u'│'.join(
            [' {} '.format(c.replace('.', ' ')) for c in r]) + u'│\n'
         for r in chunks(board_string)])
    return upper_box_line + content + lower_box_line + '\n'

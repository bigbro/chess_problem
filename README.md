Problem
=======

The problem is to find all unique configurations of a set of normal chess pieces on
a chess board with dimensions M×N where none of the pieces is in a position to take any of the
others. Assume the colour of the piece does not matter, and that there are no pawns among the
pieces.

Write a program which takes as input:
The dimensions of the board: M, N.
The number of pieces of each type (King, Queen, Bishop, Rook and Knight) to try and
place on the board.
As output, the program should list all the unique configurations to the console for which all of
the pieces can be placed on the board without threatening each other.
When returning your solution, please provide with your answer the total number of unique
configurations for a 7×7 board with 2 Kings, 2 Queens, 2 Bishops and 1 Knight.


Results
=======

To get help on program, just run it as following:

```
> python chess.py --help
usage: chess.py [-h] [-x COLUMNS] [-y ROWS] [-q QUEENS] [-b BISHOPS]
                [-r ROOKS] [-n KNIGHTS] [-k KINGS] [--verbose] [--num NUM]

Finds all unique configurations of a set of normal chess pieces on a chess
board with dimensions M*N where none of the pieces is in a position to take
any of the others.

optional arguments:
  -h, --help            show this help message and exit

board dimensions (if not set the board of 5*5 will be used):
  -x COLUMNS, --columns COLUMNS
                        columns (x-axis)
  -y ROWS, --rows ROWS  rows (y-axis)

pieces on board:
  -q QUEENS, --queens QUEENS
                        number of Queens
  -b BISHOPS, --bishops BISHOPS
                        number of Bishops
  -r ROOKS, --rooks ROOKS
                        number of Rook
  -n KNIGHTS, --knights KNIGHTS
                        number of Knights
  -k KINGS, --kings KINGS
                        number of Kings

verbosity:
  --verbose             print resulting boards
  --num NUM             number of boards to print
```

Examples
========
```
> python chess.py --rows=6 --columns=6 --verbose --queens=2 --kings=2 --bishops=2 --knights=1
┌───┬───┬───┬───┬───┬───┐
│   │   │   │ Q │   │   │
├───┼───┼───┼───┼───┼───┤
│   │   │   │   │   │ K │
├───┼───┼───┼───┼───┼───┤
│ Q │   │   │   │   │   │
├───┼───┼───┼───┼───┼───┤
│   │   │ B │   │   │   │
├───┼───┼───┼───┼───┼───┤
│   │   │   │   │   │ B │
├───┼───┼───┼───┼───┼───┤
│   │ K │   │   │   │ N │
└───┴───┴───┴───┴───┴───┘


┌───┬───┬───┬───┬───┬───┐
│ K │   │ N │   │ K │   │
├───┼───┼───┼───┼───┼───┤
│   │   │ B │   │   │   │
├───┼───┼───┼───┼───┼───┤
│   │   │   │   │   │ Q │
├───┼───┼───┼───┼───┼───┤
│   │   │   │   │   │   │
├───┼───┼───┼───┼───┼───┤
│   │ Q │   │   │   │   │
├───┼───┼───┼───┼───┼───┤
│   │   │   │ B │   │   │
└───┴───┴───┴───┴───┴───┘


┌───┬───┬───┬───┬───┬───┐
│   │   │   │   │ B │ N │
├───┼───┼───┼───┼───┼───┤
│   │ Q │   │   │   │   │
├───┼───┼───┼───┼───┼───┤
│   │   │   │   │   │   │
├───┼───┼───┼───┼───┼───┤
│   │   │   │   │   │ B │
├───┼───┼───┼───┼───┼───┤
│   │   │ Q │   │   │   │
├───┼───┼───┼───┼───┼───┤
│ K │   │   │   │ K │   │
└───┴───┴───┴───┴───┴───┘


┌───┬───┬───┬───┬───┬───┐
│   │   │ B │   │ N │   │
├───┼───┼───┼───┼───┼───┤
│   │   │   │   │   │   │
├───┼───┼───┼───┼───┼───┤
│   │   │ K │   │   │   │
├───┼───┼───┼───┼───┼───┤
│ Q │   │   │   │   │   │
├───┼───┼───┼───┼───┼───┤
│   │   │   │ Q │   │   │
├───┼───┼───┼───┼───┼───┤
│   │ B │   │   │   │ K │
└───┴───┴───┴───┴───┴───┘


┌───┬───┬───┬───┬───┬───┐
│   │   │   │ B │   │   │
├───┼───┼───┼───┼───┼───┤
│   │   │   │ B │   │ K │
├───┼───┼───┼───┼───┼───┤
│ Q │   │   │   │   │   │
├───┼───┼───┼───┼───┼───┤
│   │   │ Q │   │   │   │
├───┼───┼───┼───┼───┼───┤
│   │   │   │   │   │   │
├───┼───┼───┼───┼───┼───┤
│   │ K │   │   │   │ N │
└───┴───┴───┴───┴───┴───┘


┌───┬───┬───┬───┬───┬───┐
│   │   │   │ K │   │   │
├───┼───┼───┼───┼───┼───┤
│ B │   │   │   │   │ B │
├───┼───┼───┼───┼───┼───┤
│   │   │ Q │   │   │   │
├───┼───┼───┼───┼───┼───┤
│   │   │   │   │   │ N │
├───┼───┼───┼───┼───┼───┤
│   │ Q │   │   │   │   │
├───┼───┼───┼───┼───┼───┤
│   │   │   │ K │   │   │
└───┴───┴───┴───┴───┴───┘


┌───┬───┬───┬───┬───┬───┐
│ K │   │ N │   │   │   │
├───┼───┼───┼───┼───┼───┤
│   │   │ B │   │   │   │
├───┼───┼───┼───┼───┼───┤
│   │   │   │   │   │ Q │
├───┼───┼───┼───┼───┼───┤
│   │   │   │ K │   │   │
├───┼───┼───┼───┼───┼───┤
│   │ B │   │   │   │   │
├───┼───┼───┼───┼───┼───┤
│   │   │   │   │ Q │   │
└───┴───┴───┴───┴───┴───┘


┌───┬───┬───┬───┬───┬───┐
│   │ Q │   │   │   │   │
├───┼───┼───┼───┼───┼───┤
│   │   │   │   │   │ Q │
├───┼───┼───┼───┼───┼───┤
│ B │   │ K │   │   │   │
├───┼───┼───┼───┼───┼───┤
│   │   │   │   │   │   │
├───┼───┼───┼───┼───┼───┤
│ K │   │   │   │   │   │
├───┼───┼───┼───┼───┼───┤
│   │   │ B │   │ N │   │
└───┴───┴───┴───┴───┴───┘


┌───┬───┬───┬───┬───┬───┐
│   │ B │   │   │   │   │
├───┼───┼───┼───┼───┼───┤
│   │ N │   │   │ B │   │
├───┼───┼───┼───┼───┼───┤
│   │   │   │   │   │   │
├───┼───┼───┼───┼───┼───┤
│   │   │   │   │   │ Q │
├───┼───┼───┼───┼───┼───┤
│ Q │   │   │   │   │   │
├───┼───┼───┼───┼───┼───┤
│   │   │ K │   │ K │   │
└───┴───┴───┴───┴───┴───┘


┌───┬───┬───┬───┬───┬───┐
│   │ Q │   │   │   │   │
├───┼───┼───┼───┼───┼───┤
│   │   │   │ B │   │   │
├───┼───┼───┼───┼───┼───┤
│   │   │   │   │   │ Q │
├───┼───┼───┼───┼───┼───┤
│ K │   │ K │   │   │   │
├───┼───┼───┼───┼───┼───┤
│   │   │   │   │   │   │
├───┼───┼───┼───┼───┼───┤
│   │   │   │ B │ N │   │
└───┴───┴───┴───┴───┴───┘


┌───┬───┬───┬───┬───┬───┐
│   │   │ Q │   │   │   │
├───┼───┼───┼───┼───┼───┤
│ K │   │   │   │ B │   │
├───┼───┼───┼───┼───┼───┤
│   │   │   │   │   │   │
├───┼───┼───┼───┼───┼───┤
│ K │   │   │   │ B │   │
├───┼───┼───┼───┼───┼───┤
│   │   │   │   │ N │   │
├───┼───┼───┼───┼───┼───┤
│   │ Q │   │   │   │   │
└───┴───┴───┴───┴───┴───┘


23752 unique configurations found (first 10 are shown above)

```
